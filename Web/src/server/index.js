const express = require('express')
const sqlite3 = require('sqlite3').verbose()
const bodyParser = require('body-parser')
const path = require('path')

const app = express()
app.use(bodyParser.json())

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*'])
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.append('Access-Control-Allow-Headers', 'Content-Type')
    next();
})

const db = new sqlite3.Database(path.resolve(__dirname, 'db.sqlite'))

db.serialize(() => {
    db.run('CREATE TABLE IF NOT EXISTS users (id integer primary key autoincrement, \
         email text, login text, password text, license text, name text, surname text, \
         phone_number text)')
    db.run('CREATE TABLE IF NOT EXISTS orders (id integer primary key autoincrement, \
         client_id integer, contractor_id integer DEFAULT 0, longitude double, latitude double, \
         address text, price integer, garbage_type text, garbage_mass double, \
         additional_info text, status text)')
})

app.post('/placeOrder', (req, res) => {
    const data = req.body
    
    if (Object.keys(data).filter(param => param != 'contractor_id').reduce((acc, param) => acc
     && data[param].toString().length > 0, true)) {
        db.run('INSERT INTO orders (client_id, contractor_id, longitude, latitude, address, \
            price, garbage_type, garbage_mass, additional_info, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [data.client_id, data.contractor_id || 0, data.longitude, data.latitude, 
            data.address, data.price, data.garbage_type,
            data.garbage_mass, data.additional_info, data.status],
            (err) => console.log(err))
    }
    res.sendStatus(200)
    // FIXME: Проверка введенных данных
})

app.post('/register', (req, res) => {
    const data = req.body

    if (data.email && data.password && data.login) {
        db.get('SELECT * FROM users WHERE email=? or login=?',
         [data.email, data.login], (err, row) => {
            if (row) {
                res.sendStatus(403)
            } else if (err) {
                res.sendStatus(400)
            } else {
                db.run('INSERT INTO users (email, login, password, license,\
                    name, surname) VALUES (?, ?, ?, ?, ?, ?)',
                    [data.email, data.login , data.password,
                    data.license, data.name, data.surname])
                res.sendStatus(200)
            }
        })
    } else res.send(401)
})

app.get('/order', (req, res) => {
    data = req.query

    if (data.client_id)
        db.all('SELECT * FROM orders WHERE client_id=?', [data.client_id],
        (err, rows) => {
            if (err) res.sendStatus(403)
            else if (rows) res.send(rows) 
            else res.sendStatus(404)
        })
    else if (data.contractor_id)
        db.all('SELECT * FROM orders WHERE contractor_id=0', (err, rows) => {
            if (err) res.sendStatus(403)
            else if (rows) res.send(rows)
            else res.send(404)
        })
    else res.sendStatus(401)
})

app.post('/changeStatus', (req, res) => {
    const data = req.query
    
    db.run('UPDATE orders SET status=(?) WHERE id=(?)',
    [data.new_status, data.id],
    (err) => console.log(err))
    res.sendStatus(200)
})

app.post('/login', (req, res) => {
    const data = req.body

    db.get('SELECT * FROM users WHERE email=(?) and password=(?)',
     [data.email, data.password], (err, row) => {
        if (err) {
            console.log('DB error')
            res.send(400)
        } else if (row) {
            res.send(row)
        } else {
            res.sendStatus(404)
        }
    }
)})

app.get('/test', (req, res) => {
    db.all('SELECT * FROM orders', (err, rows) => res.send(rows))
})

app.get('/user', (req, res) => {
    data = req.query
    
    db.get('SELECT * FROM users WHERE id=(?)', [data.id], (err, row) => {
        if (err) res.sendStatus(403)
        else if (row) res.send(row)
        else res.sendStatus(404)
    })
})

app.listen('3000')