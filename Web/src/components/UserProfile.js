import React, { Component } from 'react'
import axios from 'axios'

export default class UserProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userInfo: {}
        }
    }

    componentDidMount() {
        axios.get(`http://localhost:3000/user?id=${sessionStorage.getItem('user_id')}`)
        .then(res => this.setState({userInfo: res.data}))
    }
    
    render() {
        const info = this.state.userInfo
        return(
            <div className='user-profile'>
                <h1>{info.name} {info.surname}</h1>
                <h2>{info.email}</h2>
            </div>
        )
    }
} 
