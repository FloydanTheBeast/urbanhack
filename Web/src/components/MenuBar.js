import React from 'react'
import { Link } from 'react-router-dom'

export default () => {
    return(
        <div className='menubar'>
            <div className='logo'>
                <Link to='/'>Trash Gett</Link>
            </div>
            <div>
                <Link to='/orders'>Заказы</Link>
            </div>
            <div>
                <Link to='/profile'>Профиль</Link>
            </div>
            <div>
                <Link to='/about'>О нас</Link>
            </div>
            <div className='exit-btn'>
                <Link onClick={(e) => {
                    e.preventDefault()
                    sessionStorage.clear()
                    location.reload()
                }}to='/'>Выход</Link>
            </div>
        </div>
    )
}