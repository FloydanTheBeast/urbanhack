import React, {Component} from 'react'
import ReactDOM from "react-dom"
import config from "./config"
const axios = require('axios')


class PopInput extends Component {

    constructor(props) {
        super(props)
    }

    handleClick = () => {
        let params = {
            "address": ReactDOM.findDOMNode(this.refs.address).value || '',
            "garbage_type": ReactDOM.findDOMNode(this.refs.garbage).value || '',
            "price": ReactDOM.findDOMNode(this.refs.price).value || 0,
            "garbage_mass": ReactDOM.findDOMNode(this.refs.mass).value || 0,
            "client_id": sessionStorage.getItem("user_id"),
            "latitude": this.props.lat,
            "longitude": this.props.lng

        }
        console.log('place order')
        axios.post(config.address + "/placeOrder", params).then((res) => console.log(res))

    }

    render() {
        return (
            <div className="pop" style= {{position:"absolute", left: this.props.x+'px', top: this.props.y+'px'}} >

                <span className="address">{this.props.address}</span>
                <input type="hidden" ref="address" value={this.props.address}/>
                <input ref="garbage" type="text" className="field" placeholder="Введите тип мусора"/>
                <input ref="price" className="field" type="number" placeholder="Цена"/>
                <input ref="mass" className="field" type="number" placeholder="Масса"/>
                <input ref="type" className="field" type="text" placeholder="Комментарий"/>
                <button type="submit" onClick={this.handleClick} className="accept-button">Отправить</button>
            </div>
        )
    }
}
export default PopInput