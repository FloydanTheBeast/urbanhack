import React from 'react'
import GMaps from "./GMaps"
import config from "./config";
const axios = require('axios')

 class GMapsComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "marker" : [],
            "address": "",
            "isClicked": false,
            data:[]
        }
        let x, y = 0;

    }

    componentDidMount() {
        document.addEventListener('click', (e) => {
            this.setState({isClicked: true})
            this.x = e.screenX-200
            this.y = e.screenY-230})
        let params = !sessionStorage.getItem("license")? {client_id: sessionStorage.getItem("user_id")}:
            {contractor_id: sessionStorage.getItem("user_id")}
        console.log(params)
        axios.get(config.address+"/order", {params: params }).then((res) =>  {
            console.log("points", res)
            console.log("sess", sessionStorage.getItem("license"))
            this.setState({data :res.data}) })
    }

    handleClick = (event) =>{
        let lat = event.latLng.lat(), lng = event.latLng.lng()
        let geocoder = new window.google.maps.Geocoder();
        geocoder.geocode( {'location': { lat: lat, lng: lng}}, (results, status) =>{
            if (status == 'OK') {
                this.setState({"marker": [lat, lng], "address" : results[0]['formatted_address']})
            }
            else {
                this.setState({"marker": [lat, lng], "address" : "Знамо-где"})
            }
        })
        axios.get(config.address+"/order", {params: params }).then((res) =>  {
            this.setState({data :res.data}) })
    }
     render() {
         return (
             <GMaps
                 data ={this.state.data}
                    onClick = {(e) => {this.handleClick(e)}}
                    marker={this.state.marker}
                    address = {this.state.address}
                    x = {this.x}
                    y = {this.y}

                    isClicked = {this.state.isClicked}
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyB34vqkGQ-Z9ZJdPBipq6Hr2wFEqOcw7j8"
                    loadingElement={<div style={{height: `100%`}}/>}
                    containerElement={<div style={{height: `100vh`}}/>}
                    mapElement={<div style={{height: `100%`}}/>} />
         )
     }
 }

export default GMapsComponent

