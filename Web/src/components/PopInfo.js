import React, {Component} from 'react'

class PopInfo extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="pop" >
                <span className="address">{this.props.address}</span>
                <span className="garbage">{this.props.garbage}</span>
                <span className="price">{this.props.price}</span>
                <span className="mass">{this.props.mass}</span>
                {sessionStorage.getItem("license")? <button className="accept-button">Accept</button>: ""}
            </div>
        )
    }
}

export default PopInfo