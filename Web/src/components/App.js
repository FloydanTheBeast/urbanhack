import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import MenuBar from './MenuBar'
import GMapsComponent from "./GMapsComponent"
import UserProfile from './UserProfile'
import LoginRegisterForm from "./Login"
import OrderList from './OrderList'


class App extends  React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoggedIn: false
        }
    }

render () {
    return (
        <BrowserRouter basename='/'>
            <div>
                <MenuBar />
                <Switch>
                    <Route exact path='/' render={() =>(
                         sessionStorage.getItem("user_id")? <GMapsComponent/>:<LoginRegisterForm onLogin={this.onLogin.bind(this)}/>
                    )}/>
                    <Route exact path='/orders' component={OrderList} />
                    <Route exact path='/about' render={() => (
                        <div className='about'>
                            <h1>В скором времени тут появится информация о нас...</h1>
                        </div>
                    )}/>
                    <Route exact path='/profile' render={() => (
                        <UserProfile />
                    )} />
                </Switch>
            </div>
        </BrowserRouter>
    )
}

onLogin() {
        this.setState({
            isLoggedIn: true
        })
}
}
export default App