import React from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import PopInfo from './PopInfo'
import PopInput from './PopInput'
import config from "./config"
const axios = require('axios')

const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");

let lng = 37.615660, lat = 55.750114

if("geolocation" in navigator) {
    navigator.geolocation.getCurrentPosition(function (position) {
        lat = position.coords.latitude
        lng = position.coords.longitude
    });
}


const GMaps = withScriptjs(withGoogleMap((props) =>
    <GoogleMap
        defaultZoom={15}
        defaultCenter={ { lat: lat, lng: lng}}
        onClick = {props.onClick}
    >
        {!sessionStorage.getItem("license")?
        props.marker.length > 0?
            <div>
                <Marker position={{lat: props.marker[0], lng: props.marker[1]}}/>
                <PopInput address={props.address} x={props.x} y={props.y} lat= {props.marker[0]} lng={props.marker[1]}/>
            </div>

        :"":"" }
        {props.data.map((item) =>
            <MarkerWithLabel position={{lat: item.latitude, lng: item.longitude}}
                             labelAnchor={new google.maps.Point(100, 100)}>
                <PopInfo
                    address={item.address}
                    garbage={item.garbage_type}
                    price={item.price} 
                    mass={item.garbage_mass}/>
            </MarkerWithLabel>
        )}


    </GoogleMap>
))
/* */

 export default GMaps