import React, { Component } from 'react'
import axios from 'axios'

export default class OrderList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            orderList: []
        }
        
    }

    componentDidMount() {
        axios({
            method: 'get',
            url: `http://localhost:3000/order?client_id=${sessionStorage.getItem('user_id')}`}
        ).then(res => {
            this.setState({orderList: res.data})
        })
    }

    render() {
        return (
            <div className='order-list'>
                <ul>
                    {this.state.orderList.map(order => (
                        <li key={order.id}>
                            <h2 className='address'>Адрес для вывоза: {order.address || 'Не указан'}</h2>
                            <h3 className='price'>Цена: {order.price || 'Не указана'}</h3>
                            <p className='client_id'>Id заказчика: {order.client_id || 'Не указан'}</p>
                            <p className='contractor_id'>Id исполнителя: {order.contactor_id || 'Не указан'}</p>
                            <p className='mass'>Масса мусора: {order.garbage_mass || 'Не указана'}</p>
                            <p className='garbage-type'>Тип мусора: {order.garbage_type || 'Не указан'}</p>
                            <p className='status'>Статус заказа: {order.status || 'Не указан'}</p>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}