import React from 'react'
import ReactDOM from 'react-dom'
const axios = require('axios');
import config from './config'

class LoginRegisterForm extends React.Component {

    constructor(props) {
        super(props)
        this.state={
            register: true

        }
    }
    handleLogin = () => {
        let params = {"email": ReactDOM.findDOMNode(this.refs.email).value,
            "password": ReactDOM.findDOMNode(this.refs.password).value }
        axios.post(config.address+"/login", params).then((res) => {
           sessionStorage.setItem("user_id", res.data.id)
            sessionStorage.setItem("license", res.data.license)
            this.props.onLogin()
        })
    }
    handleRegister = () => {
        let params = {"email": ReactDOM.findDOMNode(this.refs.email).value,
            "login": ReactDOM.findDOMNode(this.refs.login).value,
            "password": ReactDOM.findDOMNode(this.refs.password).value,
            "name": ReactDOM.findDOMNode(this.refs.name).value,
            "surname": ReactDOM.findDOMNode(this.refs.surname).value,
            "license": ReactDOM.findDOMNode(this.refs.license).value ==="Исполнитель" ? "20" :""
        }
        console.log(params)
        axios.post(config.address+"/register", params).then((res) => console.log(res))
    }

    render() {
        return(
            <div className="login-form">
                <div className="choose">
                    <div onClick={() => {this.setState({register:false})}}>
                        Вход
                    </div>
                    <div onClick={() => {this.setState({register:true})}}>
                        Регистрация
                    </div>
                </div>
                {
                    !this.state.register &&
                    <form>
                        <input ref="email" type="email" placeholder="Почта" required/>
                        <input  ref="password" type="password" placeholder="Пароль" required/>
                        <input type="button" value="Login" onClick={this.handleLogin}/>
                    </form>
                }
                {
                    this.state.register &&
                    <form >
                        <input ref="email" type="email" placeholder="Почта" required/>
                        <input ref="login" type="text" placeholder="Логин" required/>
                        <input ref="name" type="text" placeholder="Имя"/>
                        <input ref="surname" type="text" placeholder="Фамилия"/>
                        <input ref="password" type="password" placeholder="Пароль" required/>
                        <select ref="license">
                            <option>Заказчик</option>
                            <option>Исполнитель</option>
                        </select>
                        <input type="button" onClick={this.handleRegister} value="Register"/>
                    </form>
                }
            </div>
        )
    }
}

export default LoginRegisterForm