import React from 'react'
import { render } from 'react-dom'
import App from './components/App'
import 'normalize.css'
import './styles.sass'

render(
    <App />,
    document.getElementById('root')
)