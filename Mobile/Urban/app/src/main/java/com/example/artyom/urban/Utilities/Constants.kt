package com.example.artyom.urban.Utilities

const val LOGIN_URL = "http://172.31.29.223:3000/login"
const val REGISTER_URL = "http://172.31.29.223:3000/register"
const val GET_ORDER_URL = "http://172.31.29.223:3000/order?client_id="
const val GET_ALL_ORDERS = "http://172.31.29.223:3000/order?contractor_id="
const val SEND_ORDER_URL = "http://172.31.29.223:3000/placeOrder"
const val BROADCAST_USER_DATA_CHANGE = "broadcast_main_ui"