package com.example.artyom.urban.Adapt

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.artyom.urban.Data.Order
import com.example.artyom.urban.R
import java.text.ParseException
import java.util.ArrayList

class ordersAdapter(val context: Context, val orders: ArrayList<Order>) :  RecyclerView.Adapter<ordersAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindOrder(context, orders[position])
    }

    override fun getItemCount(): Int {
        return orders.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.order_list_item, parent, false)
        return ViewHolder(view)
    }


    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val address = itemView?.findViewById<TextView>(R.id.adress_row)
        val attr = itemView?.findViewById<TextView>(R.id.attr_row)
        val price = itemView?.findViewById<TextView>(R.id.price_row)
        val mass = itemView?.findViewById<TextView>(R.id.mass_row)
        val status = itemView?.findViewById<TextView>(R.id.status_row)

        fun bindOrder(context: Context, order: Order) {
            address?.text = order.address
            attr?.text = "исполнитель:"+order.cont_id.toString()
            price?.text = order.price.toString()
            mass?.text = order.mass.toString()
            status?.text = when(order.status){ "0" -> "Заказ создан"; "1"->"Заказ принят";"2"->"Заказ выполнен" else -> "Ожидайте"}

        }
    }
}