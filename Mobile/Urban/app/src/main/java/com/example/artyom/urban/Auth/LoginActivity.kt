package com.example.artyom.urban.Auth

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.view.View
import android.widget.Toast
import com.example.artyom.urban.R
import com.example.artyom.urban.Services.Auth
import com.example.artyom.urban.Utilities.BROADCAST_USER_DATA_CHANGE
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        enableSpinner(false)
        loginBTN.setOnClickListener {
            val email = emaitEdit.text.toString()
            val password = passEdit.text.toString()
            if(email.isNotEmpty() && password.isNotEmpty()){
                enableSpinner(true)
                Auth.loginUser(this,email,password){ success ->
                    if (success) {
                        Toast.makeText(this,"Успешно",Toast.LENGTH_SHORT).show()
                        val userDataChange = Intent(BROADCAST_USER_DATA_CHANGE)
                        LocalBroadcastManager.getInstance(this).sendBroadcast(userDataChange)
                        enableSpinner(false)
                        finish()
                    } else {
                        showError()
                        enableSpinner(false)
                    }
                }
            }
        }

        registerBTN.setOnClickListener {
            val intentReg = Intent(this,RegActivity::class.java)
            startActivity(intentReg)
        }
    }

    fun enableSpinner(enable: Boolean) {
        if (enable) {
            loginSpinner.visibility = View.VISIBLE
        } else {
            loginSpinner.visibility = View.INVISIBLE
        }
        loginBTN.isEnabled = !enable
        registerBTN.isEnabled = !enable
    }
    fun showError(){
        Toast.makeText(this,"error :c",Toast.LENGTH_SHORT).show()
    }
}
