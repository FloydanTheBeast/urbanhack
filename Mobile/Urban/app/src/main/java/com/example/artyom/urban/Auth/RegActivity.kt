package com.example.artyom.urban.Auth

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.artyom.urban.R
import com.example.artyom.urban.Services.Auth
import kotlinx.android.synthetic.main.activity_reg.*

class RegActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg)
        spinnerReg.visibility = View.GONE
        if(!switchR.isChecked){
            license.visibility = View.GONE
        } else {
            license.visibility = View.VISIBLE
        }
        createUserBtn.setOnClickListener {
            val Email = email.text.toString()
            val Name = name.text.toString()
            val Login = login.text.toString()
            val Password = password.text.toString()
            val Surname = surname.text.toString()
            val Phone = phone.text.toString()
            val License = if(switchR.isChecked)license.text.toString() else ""

            if(Email.isNotEmpty() && Name.isNotEmpty() &&
                    Login.isNotEmpty() && Password.isNotEmpty() &&
                    Surname.isNotEmpty() && Phone.isNotEmpty()){
                Auth.registerUser(
                        context = this,
                        email = Email,
                        password = Password,
                        name = Name,
                        surname = Surname,
                        login = Login,
                        certi = License
                ){success ->
                    if (success) {
                        Toast.makeText(this,"Успешно", Toast.LENGTH_SHORT).show()
                        enableSpinner(false)
                        finish()
                    } else {
                        showError()
                        enableSpinner(false)
                    }}
            }
        }
    }
    fun OnSwitch(view: View){
        if(!switchR.isChecked){
            license.visibility = View.GONE
        } else {
            license.visibility = View.VISIBLE
        }
    }

    fun enableSpinner(enable: Boolean) {
        if (enable) {
            spinnerReg.visibility = View.VISIBLE
        } else {
            spinnerReg.visibility = View.INVISIBLE
        }
        switchR.isEnabled = !enable
        createUserBtn.isEnabled = !enable
    }

    fun showError(){
        Toast.makeText(this,"error :c",Toast.LENGTH_SHORT).show()
    }
}
