package com.example.artyom.urban

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.artyom.urban.Adapt.GPSTracker

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*
import java.util.*
import android.content.Intent
import com.google.android.gms.maps.model.BitmapDescriptorFactory


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, /*GoogleMap.OnMapClickListener,*/ GoogleMap.OnMapLongClickListener {
    private lateinit var geocoder: Geocoder
    private lateinit var mMap: GoogleMap
    private lateinit var gps: GPSTracker
    private lateinit var location : LatLng
    private lateinit var addressName : String

    val PERMISSION_REQUEST = 1
    private lateinit var addresses : List<Address>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        geocoder = Geocoder(this, Locale.getDefault())
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        gps = GPSTracker(this)
        gps.getLocation()
        addresses = geocoder.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);
        location = LatLng(gps.getLatitude(), gps.getLongitude())
        textAddress.text = addresses[0].getAddressLine(0)
        addressName = addresses[0].getAddressLine(0)
        Log.d("GPS:", gps.getLatitude().toString())
        buttonNext.setOnClickListener{
            val detailIntent = Intent(this,OrderDetailActivity::class.java)
            detailIntent.putExtra("LATITUDE",location.latitude)
            detailIntent.putExtra("LONGTITUDE",location.longitude)
            detailIntent.putExtra("ADDRESS",addresses[0].getAddressLine(0))
            startActivity(detailIntent)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.addMarker(MarkerOptions().position(location).title("Your current location"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15.toFloat()))
        mMap.setOnMapLongClickListener(this)

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == PERMISSION_REQUEST){
            for(i in permissions.indices){
                if(grantResults[i] == PackageManager.PERMISSION_DENIED){
                    var requestAgain = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale(permissions[i])
                    if(requestAgain){
                        Toast.makeText(this,"Недостаточно прав",Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this,"Перейдите в настройки и предоставьте приложению разрещения",Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onMapLongClick(latLng: LatLng) {
        mMap.clear()
        location = latLng

        //Add marker on LongClick position
        val markerOptions = MarkerOptions().position(latLng).title(latLng.toString())
        mMap.addMarker(markerOptions)
        geocoder = Geocoder(this, Locale.getDefault())
        addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1);
        textAddress.text = addresses[0].getAddressLine(0)
        addressName = addresses.get(0).getAddressLine(0)
        buttonNext.setOnClickListener{
            val detailIntent = Intent(this,OrderDetailActivity::class.java)
            detailIntent.putExtra("LATITUDE",location.latitude)
            detailIntent.putExtra("LONGTITUDE",location.longitude)
            detailIntent.putExtra("ADDRESS",addresses[0].getAddressLine(0))
            startActivity(detailIntent)
        }
    }
}
