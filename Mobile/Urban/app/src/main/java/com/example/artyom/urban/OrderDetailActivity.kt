package com.example.artyom.urban

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.artyom.urban.Services.Auth
import kotlinx.android.synthetic.main.activity_order_detail.*

class OrderDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)

        var longitude = intent.getDoubleExtra("LONGTITUDE",0.0)
        var latitude = intent.getDoubleExtra("LATITUDE",0.0)
        var address = intent.getStringExtra("ADDRESS")


        textAddressOrder.text = address

        buttonSubmit.setOnClickListener {
            Auth.sendOrder(
                    context = this,
                    longitude = longitude,
                    latitude = latitude,
                    address = address,
                    price = Integer.parseInt(priceText.text.toString()),
                    type = typeText.text.toString(),
                    mass =Integer.parseInt(massText.text.toString()),
                    comm = commentText.text.toString()
            ) { _ ->
                    Toast.makeText(this,"Готово",Toast.LENGTH_SHORT).show()
                    val intent = Intent(this,MainActivity::class.java)
                    startActivity(intent)
            }
        }
    }
}
