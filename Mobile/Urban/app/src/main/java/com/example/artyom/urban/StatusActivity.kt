package com.example.artyom.urban

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.example.artyom.urban.Adapt.ordersAdapter
import com.example.artyom.urban.Data.Order
import com.example.artyom.urban.Services.Auth
import kotlinx.android.synthetic.main.activity_status.*

class StatusActivity : AppCompatActivity() {
    lateinit var ordersAdapter: ordersAdapter
    private var orders = ArrayList<Order>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)
        try {
             Auth.getOrdersById(this) {orders->
                 setOrders(orders)
            }
        } catch (e:Exception){
            Toast.makeText(this, "ошибка", Toast.LENGTH_SHORT).show()
        }
        ordersAdapter = ordersAdapter(this,orders)
        orders_recycler.adapter = ordersAdapter
        val layoutManager = LinearLayoutManager(this)
        orders_recycler.layoutManager = layoutManager
        get_ordersBTN.setOnClickListener {
            ordersAdapter = ordersAdapter(this,orders)
            orders_recycler.adapter = ordersAdapter
            val layoutManager = LinearLayoutManager(this)
            orders_recycler.layoutManager = layoutManager
        }
    }

    private fun setOrders(o:ArrayList<Order>){
        orders = o
    }
}
