package com.example.artyom.urban.Data

class Order {
    var cont_id : Int = 0
    var latitude : Double = 0.0
    var longtitude : Double = 0.0
    var address : String = ""
    var price : Int = 0
    var type : String = ""
    var mass : Double = 0.0
    var comment : String = ""
    var status: String = ""

    constructor(id : Int, latitude : Double, longitude : Double, address : String, price : Int, type : String, mass : Double,status:String, comment: String){
        this.cont_id = id
        this.latitude = latitude
        this.longtitude = longitude
        this.address = address
        this.price = price
        this.type = type
        this.mass = mass
        this.status = status
        this.comment = comment
    }

}