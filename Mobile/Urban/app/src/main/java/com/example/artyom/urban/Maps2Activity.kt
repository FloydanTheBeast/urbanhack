package com.example.artyom.urban

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.artyom.urban.Adapt.GPSTracker

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*
import java.util.*
import android.content.Intent
import android.view.View
import com.example.artyom.urban.Data.Order
import com.example.artyom.urban.Services.Auth
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.activity_maps2.*
import kotlin.collections.ArrayList


class Maps2Activity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapClickListener, /*GoogleMap.OnMapLongClickListener*/ GoogleMap.OnMarkerClickListener{
    private lateinit var mMap: GoogleMap
    private lateinit var location : LatLng
    private lateinit var markersArray : ArrayList<Order>
    private lateinit var geocoder: Geocoder
    private lateinit var gps: GPSTracker
//    private lateinit var addressName : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps2)
        markersArray = arrayListOf()
        try {
            Auth.getOrdersById(this) { orders->
                Toast.makeText(this, "получено, ${orders.size}", Toast.LENGTH_SHORT).show()
                setOrders(orders)
            }
        } catch (e:Exception){
            Toast.makeText(this, "ошибка", Toast.LENGTH_SHORT).show()
        }
        //markersArray.add(Order(1,0.0,1.1,"SanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanyaSanya",100,"Sanya",100.0,"-","comm"))
        //markersArray.add(Order(1,60.0,61.1,"Sanya",100,"Sanya",100.0,"-","comm"))
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        cardInfo.visibility = View.GONE
        buttonNext2.visibility = View.GONE
        gps = GPSTracker(this)
        gps.getLocation()
        location = LatLng(gps.getLatitude(), gps.getLongitude())
        buttonUpdate2.setOnClickListener {
            if(this::mMap.isInitialized){
                for(i in markersArray) {
                    location = LatLng(i.latitude,i.longtitude)
                    mMap.addMarker(MarkerOptions().position(location).title(i.address))
                    println("create marker")
                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMapClickListener(this)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,15.toFloat()))
        for(i in markersArray) {
            location = LatLng(i.latitude,i.longtitude)
            mMap.addMarker(MarkerOptions().position(location).title(i.address))
            println("create marker")
        }
    }

    fun setOrders(o:ArrayList<Order>){
        markersArray = o
    }
    override fun onMapClick(p0: LatLng?) {

        cardInfo.visibility = View.GONE
        buttonNext2.visibility = View.GONE
    }


    override fun onMarkerClick(marker: Marker): Boolean {
        Log.i("GoogleMapActivity", "onMarkerClick")
//        Toast.makeText(applicationContext,
//                "Marker Clicked: " + marker.title, Toast.LENGTH_LONG)
//                .show()
        markersArray.forEach {
            if(it.address == marker.title){
                cardAddress.text = it.address
                cardMass.text = it.mass.toString() + " кг"
                cardPrice.text = it.price.toString()+ " руб"
                cardType.text = it.type
                cardComment.text = "Комментарий: " + it.comment
            }

        }
        cardInfo.visibility = View.VISIBLE
        buttonNext2.visibility = View.VISIBLE
        return false
    }

}
