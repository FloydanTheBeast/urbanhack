package com.example.artyom.urban

import android.app.Application
import com.example.artyom.urban.Utilities.Prefs

class App : Application() {

    companion object {
        lateinit var prefs: Prefs
    }

    override fun onCreate() {
        prefs = Prefs(applicationContext)
        super.onCreate()
    }
}