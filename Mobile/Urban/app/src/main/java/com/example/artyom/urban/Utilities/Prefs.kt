package com.example.artyom.urban.Utilities

import android.content.Context
import android.content.SharedPreferences
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class Prefs(context: Context) {

    val PREFS_FILENAME = "prefs"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)
    val IS_LOGGED_IN = "isLoggedIn"
    val USER_PASS = "password"
    val USER_EMAIL = "userEmail"
    val USER_NAME = "name"
    val USER_SURNAME = "surname"
    val USER_LOGIN = "login"
    val LICENSE = "license"
    val USER_ID = "userId"
    var isLoggedIn: Boolean
        get() = prefs.getBoolean(IS_LOGGED_IN, false)
        set(value) {
            prefs.edit().putBoolean(IS_LOGGED_IN, value).apply()
            if(!value){
                prefs.edit().putString(USER_PASS, "").apply()
                prefs.edit().putString(USER_NAME, "").apply()
                prefs.edit().putString(USER_EMAIL, "").apply()
                prefs.edit().putString(USER_SURNAME, "").apply()
                prefs.edit().putString(USER_LOGIN, "").apply()
                prefs.edit().putString(LICENSE, "").apply()
            }
        }
    var userPassword: String
        get() = prefs.getString(USER_PASS, "")
        set(value) = prefs.edit().putString(USER_PASS, value).apply()

    var userEmail: String
        get() = prefs.getString(USER_EMAIL, "")
        set(value) = prefs.edit().putString(USER_EMAIL, value).apply()
    var userName: String
        get() = prefs.getString(USER_NAME, "")
        set(value) = prefs.edit().putString(USER_NAME, value).apply()
    var userSurname: String
        get() = prefs.getString(USER_SURNAME, "")
        set(value) = prefs.edit().putString(USER_SURNAME, value).apply()
    var userLogin: String
        get() = prefs.getString(USER_LOGIN, "")
        set(value) = prefs.edit().putString(USER_LOGIN, value).apply()
    var userLicense: String
        get() = prefs.getString(LICENSE, "")
        set(value) = prefs.edit().putString(LICENSE, value).apply()

    var userId: String
        get() = prefs.getString(USER_ID, "")
        set(value) = prefs.edit().putString(USER_ID, value).apply()
//    val requestQueue: RequestQueue = Volley.newRequestQueue(context)
}