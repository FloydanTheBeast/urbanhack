package com.example.artyom.urban

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.example.artyom.urban.Auth.LoginActivity
import com.example.artyom.urban.Utilities.BROADCAST_USER_DATA_CHANGE
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import android.widget.TextView



class MainActivity : AppCompatActivity() {
    private var permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    val PERMISSION_REQUEST = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission(this, permissions)) {
                requestPermissions(permissions, PERMISSION_REQUEST)
            }
        }
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        LocalBroadcastManager.getInstance(this).registerReceiver(userDataChangeReceiver,
                IntentFilter(BROADCAST_USER_DATA_CHANGE))
        if(!App.prefs.isLoggedIn){
            statusBTN.isEnabled = false
            map_Button.isEnabled = false
            card_view0.visibility = View.VISIBLE
        } else {
            card_view0.visibility = View.GONE
            change()
        }

        map_Button.setOnClickListener {
            if(App.prefs.isLoggedIn){
                if(App.prefs.userLicense == ""){
                    val mapsActivity = Intent(this,MapsActivity::class.java)
                    startActivity(mapsActivity)
                }else {
                    val mapsActivity = Intent(this,Maps2Activity::class.java)
                    startActivity(mapsActivity)
                }
            }
        }

        statusBTN.setOnClickListener {
            val mapsActivity = Intent(this,StatusActivity::class.java)
            startActivity(mapsActivity)
        }
    }
    fun loginRegButtonOnClick(view: View){
        if (App.prefs.isLoggedIn){
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Выход")
            builder.setMessage("вы действительно хотите выйти?")
            builder.setPositiveButton("Да"){ _, _ ->
                App.prefs.isLoggedIn = false
                statusBTN.isEnabled = false
                map_Button.isEnabled = false
                card_view0.visibility = View.VISIBLE
                change()
            }
            builder.setNegativeButton("Нет"){ _, _ ->

            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        } else {
            val loginActivity = Intent(this, LoginActivity::class.java)
            startActivity(loginActivity)
        }
    }
    private val userDataChangeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            if (App.prefs.isLoggedIn) {
                statusBTN.isEnabled = true
                map_Button.isEnabled = true
                card_view0.visibility = View.GONE
                statusBTN.isEnabled = true
                change()
            }
        }
    }

    fun change(){
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        val headerView = navigationView.getHeaderView(0)

        val navUsername = headerView.findViewById(R.id.userNavName) as TextView
        val navEmail = headerView.findViewById(R.id.userNavEmail) as TextView
        val btn = headerView.findViewById(R.id.loginRegButton) as Button

        if(App.prefs.isLoggedIn) {
            navUsername.text = App.prefs.userName + " " + App.prefs.userSurname
            navEmail.text = App.prefs.userEmail
            btn.text = "ВЫХОД"
        } else {
            navUsername.text = ""
            navEmail.text = ""
            btn.text = "ВХОД / РЕГИСТРАЦИЯ"
        }
    }
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }
    fun checkPermission(context: Context, permissionArray: Array<String>): Boolean {
        var allSuccess = true
        for (i in permissionArray.indices){
            if(checkCallingOrSelfPermission(permissionArray[i]) == PackageManager.PERMISSION_DENIED)
                allSuccess = false
        }
        return allSuccess
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == PERMISSION_REQUEST){
            for(i in permissions.indices){
                if(grantResults[i] == PackageManager.PERMISSION_DENIED){
                    var requestAgain = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale(permissions[i])
                    if(requestAgain){
                        Toast.makeText(this,"Недостаточно прав", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this,"Перейдите в настройки и предоставьте приложению разрещения", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}
