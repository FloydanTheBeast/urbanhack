package com.example.artyom.urban.Services

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.artyom.urban.App
import com.example.artyom.urban.Data.Order
import com.example.artyom.urban.Utilities.*
import org.json.JSONException
import org.json.JSONObject



object Auth {

    fun registerUser(context: Context,email: String, password: String,name:String,surname:String,login:String,certi:String, complete: (Boolean) -> Unit) {

        val jsonBody = JSONObject()
        jsonBody.put("email", email)
        jsonBody.put("password", password)
        jsonBody.put("name", name)
        jsonBody.put("surname", surname)
        jsonBody.put("login", login)
        jsonBody.put("license", certi)
        val requestBody = jsonBody.toString()
        val registerRequest = object : StringRequest(Method.POST, REGISTER_URL, Response.Listener { response ->
            complete(true)
        }, Response.ErrorListener { error ->
            Log.d("ERROR", "Could not register user: $error")
            complete(false)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray {
                return requestBody.toByteArray()
            }
        }

        Volley.newRequestQueue(context).add(registerRequest)
    }

    fun loginUser(context: Context,email: String, password: String, complete: (Boolean) -> Unit) {

        val jsonBody = JSONObject()
        jsonBody.put("email", email)
        jsonBody.put("password", password)
        val requestBody = jsonBody.toString()
        val loginRequest =object : JsonObjectRequest(Request.Method.POST, LOGIN_URL, null, Response.Listener { response ->
            try {
                App.prefs.userId = response.getString("id")
                App.prefs.userEmail = response.getString("email")
                App.prefs.userPassword = response.getString("password")
                App.prefs.userName = response.getString("name")
                App.prefs.userSurname = response.getString("surname")
                App.prefs.userLogin = response.getString("login")
                App.prefs.userLicense = response.getString("license")
                App.prefs.isLoggedIn = true
                complete(true)
            } catch (e: JSONException) {
                Log.d("JSON", "EXC:" + e.localizedMessage)
                complete(false)
            }
        }, Response.ErrorListener { error ->
            Log.d("ERROR", "Could not login user: $error")
            complete(false)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
            override fun getBody(): ByteArray {
                return requestBody.toByteArray()
            }
        }
        Volley.newRequestQueue(context).add(loginRequest)
    }
     fun getOrdersById(context: Context,orders :(ArrayList<Order>)-> Unit) {
        var userOrders = ArrayList<Order>()
        val getOrdersRequest =object : JsonArrayRequest(Request.Method.GET, GET_ORDER_URL+"1", null, Response.Listener { response ->
            try {
                for (i in 0 until response.length()) {
                    val orderW = response.getJSONObject(i)
                    val order = Order(
                            address=  orderW.getString("address"),
                            id = 0,//CHANGE LATER
                            latitude = orderW.getDouble("latitude"),
                            longitude = orderW.getDouble("longitude"),
                            price = orderW.getInt("price"),
                            type = orderW.getString("garbage_type"),
                            mass = orderW.getDouble("garbage_mass"),
                            comment = orderW.getString("additional_info"),
                            status = ""
                    )
                    userOrders.add(order)
                    println(order.latitude)
                    print(order.longtitude)
                }
                orders(userOrders)
        } catch (e: JSONException) {
                Log.d("JSON", "EXC:" + e.localizedMessage)
            }
        }, Response.ErrorListener { error ->
            Log.d("ERROR", "Could not find orders. $error")
        }) {

            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }
        }
        Volley.newRequestQueue(context).add(getOrdersRequest)
        println(userOrders)
    }
    fun sendOrder(context: Context,longitude:Double,latitude:Double,address:String,price:Int,type:String,mass:Int,comm:String,complete: (Boolean) -> Unit) {
        val jsonBody = JSONObject()
        jsonBody.put("client_id", App.prefs.userId)
        jsonBody.put("longitude",longitude)
        jsonBody.put("latitude",latitude)
        jsonBody.put("address",address)
        jsonBody.put("price",price)
        jsonBody.put("garbage_type",type)
        jsonBody.put("garbage_mass",mass)
        jsonBody.put("additional_info",comm)
        jsonBody.put("status",0)
        val requestBody = jsonBody.toString()
        val registerRequest = object : StringRequest(Method.POST, SEND_ORDER_URL, Response.Listener { response ->
            complete(true)
        }, Response.ErrorListener { error ->
            Log.d("ERROR", "Could not register user: $error")
            complete(false)
        }) {
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getBody(): ByteArray {
                return requestBody.toByteArray()
            }
        }

        Volley.newRequestQueue(context).add(registerRequest)
    }

}